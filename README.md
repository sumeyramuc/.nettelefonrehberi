
### What is this repository for? ###
Public ve Admin arayüzlerine sahip bu Web projesinde; Asp.net, MsSql, Razor ve Entity Framework kullanılmıştır.
-Admin giriş yaptıktan sonra kayıtlı rehberi görebilmekte. Kayıtlı kişileri düzenleyebilir, silebilir, detaylı bilgi alabilir
ve yeni kayıt ekleyebilir. 
-Silme işleminde silinecek kaydın başka bir kayıtta yönetici konumunda olmaması gerekmektedir. Aksi durumda silme işlemi
gerçekleştirilemez.
-Public arayüzünde kayıtlı kullanıcıların genel bilgileri ve detaylı bilgileri mevcuttur.
