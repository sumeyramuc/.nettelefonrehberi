﻿//SUMEYRAMUCAHIOGLU
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using TelefonRehberi.Areas.Admin.Models.DB;
namespace TelefonRehberi.Areas.Admin.Models.ViewModel
{
   
    public class CalisansController : Controller
    {
        private TelefonRehberiEntities db = new TelefonRehberiEntities();

        public ActionResult Index2()
        {
            return View(db.Calisans);
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (String.IsNullOrEmpty(HttpContext.User.Identity.Name))
            {
                FormsAuthentication.SignOut();
                return View();
            }
            return Redirect("/Calisans/Index2");


        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model, string returnurl)
        {
            if (ModelState.IsValid)
            {
               var sifre= Crypto.Hash(model.Password);
                var admin = db.AdminDatas.Where(ww => ww.Name == model.Name && ww.Password == sifre);
                //Aşağıdaki if komutu gönderilen mail ve şifre doğrultusunda kullanıcı kontrolu yapar. Eğer kullanıcı var ise login olur.
                if (admin.Count() > 0)
                {
                    FormsAuthentication.SetAuthCookie(model.Name, true);
                    return RedirectToAction("Index","Calisans");
                }

                else
                {
                    ModelState.AddModelError("", "Kullanıcı Adı veya şifre hatalı!");
                }
            }
            return View(model);
        }
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index2", "Calisans");
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPassword(string EmailID)
        {
            //Verify Email ID
            //Generate Reset password link 
            //Send Email 
            string message = "";
            bool status = false;

            using (db)
            {
                var account = db.AdminDatas.Where(a => a.EmailID == EmailID).FirstOrDefault();
                if (account != null)
                {
                    //Send email for reset password
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.EmailID, resetCode, "ResetPassword");
                    account.ResetPasswordCode = resetCode;
                    //This line I have added here to avoid confirm password not match issue , as we had added a confirm password property 
                    //in our model class in part 1
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    message = "Reset password link has been sent to your email id.";
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        public ActionResult ResetPassword(string id)
        {
            //Verify the reset password link
            //Find account associated with this link
            //redirect to reset password page
            if (string.IsNullOrWhiteSpace(id))
            {
                return HttpNotFound();
            }

            using (db)
            {
                var user = db.AdminDatas.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (db)
                {
                    var user = db.AdminDatas.Where(a => a.ResetPasswordCode == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = Crypto.Hash(model.NewPassword);
                        user.ResetPasswordCode = "";
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        message = "New password updated successfully";
                    }
                }
            }
            else
            {
                message = "Something invalid";
            }
            ViewBag.Message = message;
            return View(model);
        }

        // GET: Calisans
        [_SessionControl]
        public ActionResult Index()
        {
            return View(db.Calisans.ToList());
        }

        // GET: Calisans/Details/5
        [_SessionControl]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calisan calisan = db.Calisans.Find(id);
            if (calisan == null)
            {
                return HttpNotFound();
            }
            return View(calisan);
        }
        public ActionResult Details2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calisan calisan = db.Calisans.Find(id);
            if (calisan == null)
            {
                return HttpNotFound();
            }
            return View(calisan);
        }

        // GET: Calisans/Create
        [_SessionControl]
        public ActionResult Create()
        {
            ViewBag.Departments = new SelectList(db.Calisans.Select(model => model.Departman).Distinct(), "Departman");
            ViewBag.Yoneticis = new SelectList(db.Calisans.Select(model => model.Yonetici).Distinct(), "Yonetici");
            return View();
        }

        // POST: Calisans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [_SessionControl]
        public ActionResult Create([Bind(Include = "Ad,Soyad,Telefon,Departman,Yonetici")] Calisan calisan)
        {
            if (ModelState.IsValid)
            {
                db.Calisans.Add(calisan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(calisan);
        }

        // GET: Calisans/Edit/5
        [_SessionControl]
        public ActionResult Edit(int? id)
        {
            ViewBag.Departments = new SelectList(db.Calisans.Select(model => model.Departman).Distinct(), "Departman");
            ViewBag.Yoneticis = new SelectList(db.Calisans.Select(model => model.Yonetici).Distinct(), "Yonetici");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calisan calisan = db.Calisans.Find(id);
            if (calisan == null)
            {
                return HttpNotFound();
            }
            return View(calisan);
        }

        // POST: Calisans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [_SessionControl]
        public ActionResult Edit([Bind(Include = "Id,Ad,Soyad,Telefon,Departman,Yonetici")] Calisan calisan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(calisan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(calisan);
        }

        // GET: Calisans/Delete/5
        [_SessionControl]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calisan calisan = db.Calisans.Find(id);
            if (calisan == null)
            {
                return HttpNotFound();
            }
            return View(calisan);
        }

        // POST: Calisans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [_SessionControl]
        public ActionResult DeleteConfirmed(int id)
        {
            int i = 0;
            Calisan calisan = db.Calisans.Find(id);
            String adsoyad = db.Calisans.Find(id).Ad + db.Calisans.Find(id).Soyad;
            adsoyad=adsoyad.Trim().Replace(" ", string.Empty);
            int sayac = 0;

            List<string> list=db.Calisans.Select(model => model.Yonetici).ToList();
            foreach (string s in list)
            {
                
                if (string.Equals(s.Trim().Replace(" ", string.Empty), adsoyad, StringComparison.CurrentCultureIgnoreCase))
                {
                    System.Diagnostics.Debug.WriteLine(adsoyad + s.Trim().Replace(" ", string.Empty));
                    sayac++;
                }
                
            }
            if (sayac > 0)
            {
                //ViewBag.Message = "Bu kişi başka bir alanda yönetici konumundadır. Silme işlemi yapılamaz!";
                TempData["Message1"] = "Bu kişi başka bir alanda yönetici konumundadır. Silme işlemi yapılamaz!!";
            }
            else
            {
                db.Entry(calisan).State = EntityState.Deleted;
                db.SaveChanges();
                //ViewBag.Message = "Silme işlemi tamamlanmıştır.";
                TempData["Message1"] = "Silme işlemi tamamlanmıştır";

            }

            return RedirectToAction("Index");
        }
    
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/Calisans/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("******", "Dotnet Awesome"); //Replace with actual mail
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "******"; // Replace with actual password

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Your account is successfully created!";
                body = "<br/><br/>We are excited to tell you that your Dotnet Awesome account is" +
                    " successfully created. Please click on the below link to verify your account" +
                    " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/>br/>We got request for reset your account password. Please click on the below link to reset your password" +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
    }
}
