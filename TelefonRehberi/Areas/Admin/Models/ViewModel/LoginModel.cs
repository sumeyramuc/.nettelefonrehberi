﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Areas.Admin.Models.ViewModel
{
    public class LoginModel
    {
        
        [Required(ErrorMessage = "Lütfen kullanıcı adınızı giriniz.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Lütfen şifrenizi giriniz.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
    public class LoginModelContext:DbContext
    {
        public DbSet<LoginModel> LoginModel { get; set; }
        public System.Data.Entity.DbSet<TelefonRehberi.Areas.Admin.Models.DB.AdminData> AdminDatas { get; set; }
    }
}