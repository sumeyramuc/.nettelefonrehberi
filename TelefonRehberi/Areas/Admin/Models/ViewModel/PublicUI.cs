﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TelefonRehberi.Areas.Admin.Models.ViewModel
{
    public class PublicUI
    {
            [Key]
            public int Id { get; set; }

            [Required(ErrorMessage = "Bu alanı boş bırakmayınız.")]
            [StringLength(50, ErrorMessage = "Lütfen 50 karakterden fazla değer girmeyiniz")]
            public string Ad { get; set; }

            [Required(ErrorMessage = "Bu alanı boş bırakmayınız.")]
            [StringLength(50, ErrorMessage = "Lütfen 50 karakterden fazla değer girmeyiniz")]
            public string Soyad { get; set; }

            [Required(ErrorMessage = "Bu alanı boş bırakmayınız.")]
            [StringLength(20, ErrorMessage = "Lütfen 20 karakterden fazla değer girmeyiniz")]
            public string Telefon { get; set; }

            [StringLength(50, ErrorMessage = "Lütfen 50 karakterden fazla değer girmeyiniz")]
            public string Departman { get; set; }

            [StringLength(50, ErrorMessage = "Lütfen 50 karakterden fazla değer girmeyiniz")]
            public string Yonetici { get; set; }

    }
    public class PublicUIContext : DbContext
    {
        public DbSet<PublicUI> PublicUI { get; set; }
        public System.Data.Entity.DbSet<TelefonRehberi.Areas.Admin.Models.DB.Calisan> Calisans { get; set; }    }
    }